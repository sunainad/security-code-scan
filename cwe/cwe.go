package cwe

import (
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

var cweIDs = map[string]int{
	"SCS0001": 78,
	"SCS0002": 89,
	"SCS0003": 643,
	"SCS0004": 295,
	"SCS0005": 338,
	"SCS0006": 327,
	"SCS0007": 611,
	"SCS0008": 614,
	"SCS0009": 1004,
	"SCS0010": 327,
	"SCS0011": 611,
	"SCS0012": 284,
	"SCS0013": 327,
	"SCS0015": 259,
	"SCS0016": 352,
	"SCS0017": 554,
	"SCS0019": 524,
	"SCS0018": 22,
	"SCS0021": 554,
	"SCS0022": 554,
	"SCS0023": 554,
	"SCS0024": 554,
	"SCS0026": 90,
	"SCS0027": 601,
	"SCS0028": 502,
	"SCS0029": 79,
	"SCS0030": 554,
	"SCS0031": 90,
	"SCS0032": 521,
	"SCS0033": 521,
	"SCS0034": 521,
}

// severity maps CWE IDs to report.SeverityLevel.
// Severity levels are determined by taking the average of reported
// vulnerabilities listed with the same CWE ID on nvd.nist.gov.
var severity = map[int]report.SeverityLevel{
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-22+improper&search_type=all&isCpeNameSearch=false
	22: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-78+improper&search_type=all&isCpeNameSearch=false
	78: report.SeverityLevelCritical,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-79&queryType=phrase&search_type=all&isCpeNameSearch=false
	79: report.SeverityLevelMedium,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-89&queryType=phrase&search_type=all&isCpeNameSearch=false
	89: report.SeverityLevelCritical,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-90&queryType=phrase&search_type=all&isCpeNameSearch=false
	90: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-259&search_type=all&isCpeNameSearch=false
	259: report.SeverityLevelCritical,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-284&queryType=phrase&search_type=all&isCpeNameSearch=false
	284: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-295&queryType=phrase&search_type=all&isCpeNameSearch=false
	295: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-327&search_type=all&isCpeNameSearch=false
	327: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-338&search_type=all&isCpeNameSearch=false
	338: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-352&search_type=all&isCpeNameSearch=false
	352: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-502&search_type=all&isCpeNameSearch=false
	502: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-521&search_type=all&isCpeNameSearch=false
	521: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-524&search_type=all&isCpeNameSearch=false
	524: report.SeverityLevelMedium,
	// based on description
	554: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-601&search_type=all&isCpeNameSearch=false
	601: report.SeverityLevelHigh,
	// https://nvd.nist.gov/vuln/search/results?form_type=Basic&results_type=overview&query=cwe-611&queryType=phrase&search_type=all&isCpeNameSearch=false
	611: report.SeverityLevelHigh,
	// https://github.com/returntocorp/semgrep-rules/blob/develop/go/lang/security/audit/net/cookie-missing-secure.yaml
	614: report.SeverityLevelHigh,
	// based on description
	643: report.SeverityLevelMedium,
	// https://github.com/snowflakedb/snowflake-jdbc/blob/master/.semgrep.yml#L188-L206
	1004: report.SeverityLevelHigh,
}

// CodeToSeverity maps a security code scan code to a report.SeverityLevel
func CodeToSeverity(code string) report.SeverityLevel {
	id, ok := cweIDs[code]
	if !ok {
		return report.SeverityLevelUnknown
	}
	severity, ok := severity[id]
	if !ok {
		return report.SeverityLevelUnknown
	}
	return severity
}
